import 'package:deine_job_website/generated/assets.dart';
import 'package:deine_job_website/utils/app_colors.dart';
import 'package:deine_job_website/utils/size_extention/size_extention.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Tab1 extends StatefulWidget {
  const Tab1({super.key});

  @override
  State<Tab1> createState() => _Tab1State();
}

class _Tab1State extends State<Tab1> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizeInfo) {
      return Column(
        children: [
          Text(
            "Drei einfache Schritte\nzu deinem neuen Job",
            style: TextStyle(
              color: AppColors.textColor,
              fontSize: 30.sp,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 2.h,
          ),
          sizeInfo.isMobile
              ? SizedBox(
                  height: 120.h,
                  child: Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      Positioned(
                        left: -25,
                        top: 0,
                        child: Container(
                          margin: EdgeInsets.only(top: 8.h),
                          width: 45.w,
                          height: 45.w,
                          padding: EdgeInsets.only(left: 10.w),
                          alignment: Alignment.bottomLeft,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.indexBgColor,
                          ),
                        ),
                      ),
                      Positioned(
                        left: -25,
                        top: 0,
                        child: Container(
                          margin: EdgeInsets.only(top: 8.h),
                          height: 45.w,
                          padding: EdgeInsets.only(left: 10.w, bottom: 5.h),
                          alignment: Alignment.bottomLeft,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "1.",
                                textAlign: TextAlign.start,
                                style: TextStyle(color: AppColors.textLightColor, fontSize: 120.sp, height: 0.8),
                              ),
                              SizedBox(
                                width: 5.w,
                              ),
                              Text(
                                "Erstellen dein Lebenslauf",
                                style: TextStyle(color: AppColors.textLightColor, fontSize: 17.sp),
                              )
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        left: 10.w,
                        top: 0,
                        child: Container(
                          width: 80.w,
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              SvgPicture.asset(
                                Assets.assetsUndrawProfileDataReV81r,
                                width: 50.w,
                              ),
                              SizedBox(
                                height: 8.h,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        left: -30,
                        bottom: 5.h,
                        child: Container(
                          width: 320,
                          height: 320,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.indexBgColor,
                          ),
                        ),
                      ),
                      Positioned(
                        left: 30,
                        bottom: 00,
                        child: Container(
                          // height: 60.h,
                          // color: Colors.red,
                          padding: EdgeInsets.only(left: 10.w, top: 1.h),
                          alignment: Alignment.bottomLeft,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "3.",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(color: AppColors.textLightColor, fontSize: 120.sp, height: 0.8),
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(
                                    "Mit nur einem Kclick bewerben",
                                    style: TextStyle(color: AppColors.textLightColor, fontSize: 17.sp),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              SvgPicture.asset(
                                Assets.assetsUndrawPersonalFile222m,
                                width: 60.w,
                                height: 45.w,
                                fit: BoxFit.fill,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 31.h),
                        child: SizedBox(
                          height: 50.h,
                          child: ClipPath(
                            clipper: WaveClipper(),
                            child: Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    AppColors.lightGradiantColor,
                                    AppColors.secondaryColor,
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 15,
                        top: 170,
                        child: Container(
                          margin: EdgeInsets.only(top: 8.h),
                          height: 50.h,
                          padding: EdgeInsets.only(left: 10.w, bottom: 5.h),
                          alignment: Alignment.bottomLeft,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "2.",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(color: AppColors.textLightColor, fontSize: 120.sp, height: 0.8),
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(
                                    "Erstellen dein Lebenslauf",
                                    style: TextStyle(color: AppColors.textLightColor, fontSize: 17.sp),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              SvgPicture.asset(
                                Assets.assetsUndrawTask31wc,
                                width: 45.w,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Stack(
                  alignment: Alignment.center,
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 15.w),
                          child: Stack(
                            alignment: Alignment.bottomLeft,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 8.h),
                                width: 130,
                                height: 130,
                                padding: EdgeInsets.only(left: 10.w),
                                alignment: Alignment.topRight,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppColors.indexBgColor,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    SizedBox(
                                      width: 2.w,
                                    ),
                                    Text(
                                      "1.",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(color: AppColors.textLightColor, fontSize: 80.sp, height: 0.8),
                                    ),
                                    SizedBox(
                                      width: 1.w,
                                    ),
                                    Text(
                                      "Erstellen dein Lebenslauf",
                                      style: TextStyle(color: AppColors.textLightColor, fontSize: 17.sp),
                                    ),
                                    SizedBox(
                                      width: 3.w,
                                    ),
                                    SvgPicture.asset(
                                      Assets.assetsUndrawProfileDataReV81r,
                                      width: 50.w,
                                      height: 20.h,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 55.h,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 10.h),
                                    child: SizedBox(
                                      height: 35.h,
                                      width: 100.w,
                                      child: ClipPath(
                                        clipper: WaveClipper(),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              colors: [
                                                AppColors.lightGradiantColor,
                                                AppColors.secondaryColor,
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        SvgPicture.asset(
                                          Assets.assetsUndrawTask31wc,
                                          width: 50.w,
                                          height: 20.h,
                                        ),
                                        SizedBox(
                                          width: 5.w,
                                        ),
                                        Text(
                                          "2.",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(color: AppColors.textLightColor, fontSize: 80.sp, height: 0.8),
                                        ),
                                        SizedBox(
                                          width: 1.w,
                                        ),
                                        Text(
                                          "Erstellen dein Jobinserat",
                                          style: TextStyle(color: AppColors.textLightColor, fontSize: 17.sp),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 25.w),
                              child: Stack(
                                alignment: Alignment.topLeft,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 3.h),
                                    width: 170,
                                    height: 170,
                                    padding: EdgeInsets.only(left: 10.w),
                                    // alignment: Alignment.topLeft,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: AppColors.indexBgColor,
                                    ),
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      SizedBox(
                                        width: 2.w,
                                      ),
                                      Text(
                                        "3.",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(color: AppColors.textLightColor, fontSize: 80.sp, height: 0.8),
                                      ),
                                      SizedBox(
                                        width: 1.w,
                                      ),
                                      Text(
                                        "Mit nur einem Kclick\nbewerben",
                                        style: TextStyle(color: AppColors.textLightColor, fontSize: 17.sp),
                                      ),
                                      SizedBox(
                                        width: 3.w,
                                      ),
                                      SvgPicture.asset(
                                        Assets.assetsUndrawPersonalFile222m,
                                        width: 55.w,
                                        height: 20.h,
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Positioned(
                      left: 23.w,
                      top: 200,
                      child: Container(
                        height: 28.h,
                        width: 32.w,
                        child: SvgPicture.asset(
                          Assets.assetsRightSideArrow,
                          height: 18.h,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 30.w,
                      top: 550,
                      child: Container(
                        height: 22.h,
                        width: 32.w,
                        child: SvgPicture.asset(
                          Assets.assetsLeftSideArrow,
                          height: 18.h,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ],
                ),
          SizedBox(
            height: 15.h,
          )
        ],
      );
    });
  }
}

//Costom CLipper class with Path
class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height); //start path with this if you are making at bottom

    var firstStart = Offset(size.width / 5, size.height);
    //fist point of quadratic bezier curve
    var firstEnd = Offset(size.width / 1.8, size.height - 30.0);
    //second point of quadratic bezier curve
    path.quadraticBezierTo(firstStart.dx, firstStart.dy, firstEnd.dx, firstEnd.dy);

    var secondStart = Offset(size.width - (size.width / 3.2), size.height - 40);
    //third point of quadratic bezier curve
    var secondEnd = Offset(size.width, size.height - 35);
    //fourth point of quadratic bezier curve
    path.quadraticBezierTo(secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);

    path.lineTo(size.width, 0); //end with this path if you are making wave at bottom
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false; //if new instance have different instance than old instance
    //then you must return true;
  }
}
