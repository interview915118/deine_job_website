import 'package:deine_job_website/generated/assets.dart';
import 'package:deine_job_website/utils/app_colors.dart';
import 'package:deine_job_website/utils/size_extention/size_extention.dart';
import 'package:deine_job_website/view/tab_1.dart';
import 'package:deine_job_website/view/tab_2.dart';
import 'package:deine_job_website/view/tab_3.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_builder/responsive_builder.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  List<Widget> tabList = <Widget>[Tab1(), Tab2(), Tab3()];
  final ScrollController _scrollController = ScrollController();

  bool scrolledUp = false;
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizeInfo) {
        return Scaffold(
          body: NotificationListener<ScrollNotification>(
            onNotification: (scrollNotification) {
              print('inside the onNotification');
              if (_scrollController.position.userScrollDirection == ScrollDirection.reverse) {
                print('scrolled down');
                setState(() {
                  scrolledUp = false;
                });
                //the setState function
              } else if (_scrollController.position.userScrollDirection == ScrollDirection.forward) {
                print('scrolled up');
                setState(() {
                  scrolledUp = true;
                });
                //setState function
              }
              return true;
            },

            child: Stack(
              children: [
                ListView(
                  controller: _scrollController,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 6.h, bottom: 5.h),
                      height: 90.h,
                      width: double.infinity,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          ClipPath(
                            clipper: WaveClipper(),
                            child: Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    AppColors.lightGradiantColor,
                                    AppColors.secondaryColor,
                                  ],
                                ),
                              ),
                            ),
                          ),
                          sizeInfo.isMobile
                              ? Column(
                                  children: [
                                    SizedBox(
                                      height: 5.h,
                                    ),
                                    Text(
                                      "Deine Job\nWebsite",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: AppColors.textDarkColor, fontSize: 50.sp, fontWeight: FontWeight.normal),
                                    ),
                                    SizedBox(
                                      width: 10.w,
                                    ),
                                    SvgPicture.asset(
                                      Assets.assetsUndrawAgreementAajr,
                                      width: 100.w,
                                      height: 50.h,
                                      fit: BoxFit.cover,
                                    )
                                  ],
                                )
                              : Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Deine Job\nWebsite",
                                          style: TextStyle(color: AppColors.textDarkColor, fontSize: 65.sp, fontWeight: FontWeight.normal),
                                        ),
                                        SizedBox(
                                          height: 5.h,
                                        ),
                                        registerButton(),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 10.w,
                                    ),
                                    ClipOval(
                                      child: CircleAvatar(
                                        radius: 180,
                                        backgroundColor: AppColors.backgroundColor,
                                        child: SvgPicture.asset(
                                          Assets.assetsUndrawAgreementAajr,
                                          // width: 50.w,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                        ],
                      ),
                    ),
                    tabBar(),
                    tabList[selectedIndex],
                  ],
                ),
                appBar(),
              ],
            ),
          ),
          bottomNavigationBar: sizeInfo.isMobile ? bottomBar() : null,
        );
      },
    );
  }

  Widget appBar() {
    return ResponsiveBuilder(builder: (context, sizeInfo) {
      return Column(children: [
        Container(
          height: .6.h,
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [
              AppColors.primaryColor,
              AppColors.blue,
            ],
          )),
        ),
        Container(
          height: 8.h,
          width: double.infinity,
          alignment: Alignment.centerRight,
          padding: EdgeInsets.symmetric(horizontal: 2.w),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(12), bottomRight: Radius.circular(12)),
            color: AppColors.backgroundColor,
            boxShadow: [
              BoxShadow(color: Colors.grey.withOpacity(0.5), blurRadius: 6, spreadRadius: 3),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              if (!scrolledUp && sizeInfo.isDesktop)
                Row(
                  children: [
                    Text(
                      "Jetzt Klicken",
                      style: TextStyle(color: AppColors.textColor),
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    OutlinedButton(
                      onPressed: () {},
                      style: OutlinedButton.styleFrom(
                          side: BorderSide(width: 1, color: AppColors.tabBorder),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                          )),
                      child: Text(
                        "Kostenlos Registrieren",
                        style: TextStyle(color: AppColors.primaryColor),
                      ),
                    ),
                  ],
                ),
              TextButton(
                onPressed: () {},
                child: Text(
                  "Login",
                  textAlign: TextAlign.end,
                  style: TextStyle(color: AppColors.primaryColor),
                ),
              ),
            ],
          ),
        ),
      ]);
    });
  }

  Widget bottomBar() {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(bottom: 2.h),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12)),
          color: AppColors.backgroundColor,
          boxShadow: [
            BoxShadow(color: Colors.grey.withOpacity(0.5), blurRadius: 6, spreadRadius: 3),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.all(5.w),
          child: registerButton(),
        ),
      ),
    );
  }

  Widget registerButton() {
    return ResponsiveBuilder(builder: (context, sizeInfo) {
      return InkWell(
        onTap: () {},
        child: Container(
          constraints: BoxConstraints(
            maxWidth: sizeInfo.isMobile ? 100.w : 20.w,
            minWidth: sizeInfo.isMobile ? 100.w : 20.w,
          ),
          padding: EdgeInsets.symmetric(vertical: 1.h, horizontal: sizeInfo.isMobile ? 8.w : 4.w),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: LinearGradient(colors: [
                AppColors.primaryColor,
                AppColors.blue,
              ])),
          child: Text(
            "Kostenlos Registrieren",
            textAlign: TextAlign.center,
            style: TextStyle(color: AppColors.backgroundColor, fontSize: 14.sp),
          ),
        ),
      );
    });
  }

  Widget tabBar() {
    return Align(
      alignment: Alignment.center,
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 2.h),
        scrollDirection: Axis.horizontal,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: Container(
            alignment: Alignment.center,
            height: 4.5.h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(color: AppColors.tabBorder),
            ),
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    onItemTapped(0);
                  },
                  hoverColor: AppColors.secondaryColor,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    color: selectedIndex == 0 ? AppColors.tabSelection : Colors.transparent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.h),
                      child: Text(
                        "Arbeitnehmer",
                        style: TextStyle(color: selectedIndex == 0 ? AppColors.backgroundColor : AppColors.primaryColor),
                      ),
                    ),
                  ),
                ),
                VerticalDivider(
                  color: AppColors.tabBorder,
                  width: 1,
                ),
                InkWell(
                  onTap: () {
                    onItemTapped(1);
                  },
                  hoverColor: AppColors.secondaryColor,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    color: selectedIndex == 1 ? AppColors.tabSelection : Colors.transparent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.h),
                      child: Text(
                        "Arbeitgeber",
                        style: TextStyle(color: selectedIndex == 1 ? AppColors.backgroundColor : AppColors.primaryColor),
                      ),
                    ),
                  ),
                ),
                VerticalDivider(
                  color: AppColors.tabBorder,
                  width: 1,
                ),
                InkWell(
                  onTap: () {
                    onItemTapped(2);
                  },
                  hoverColor: AppColors.secondaryColor,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    color: selectedIndex == 2 ? AppColors.tabSelection : Colors.transparent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.h),
                      child: Text(
                        "Temporarburo",
                        style: TextStyle(color: selectedIndex == 2 ? AppColors.backgroundColor : AppColors.primaryColor),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//Costom CLipper class with Path
class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height); //start path with this if you are making at bottom

    var firstStart = Offset(size.width / 5, size.height);
    //fist point of quadratic bezier curve
    var firstEnd = Offset(size.width / 1.8, size.height - 30.0);
    //second point of quadratic bezier curve
    path.quadraticBezierTo(firstStart.dx, firstStart.dy, firstEnd.dx, firstEnd.dy);

    var secondStart = Offset(size.width - (size.width / 3.2), size.height - 40);
    //third point of quadratic bezier curve
    var secondEnd = Offset(size.width, size.height - 35);
    //fourth point of quadratic bezier curve
    path.quadraticBezierTo(secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);

    path.lineTo(size.width, 0); //end with this path if you are making wave at bottom
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false; //if new instance have different instance than old instance
    //then you must return true;
  }
}
