import 'package:flutter/material.dart';

class AppColors {
  AppColors._();
  static const Color primaryColor = Color(0xFF319795);
  static const Color secondaryColor = Color(0xFFE6FFFA);
  static const Color backgroundColor = Color(0xFFFFFFFF);
  static const Color textColor = Color(0xFF4A5568);
  static const Color textDarkColor = Color(0xFF2D3748);
  static const Color textLightColor = Color(0xFF718096);
  static const Color lightGradiantColor = Color(0xFFEBF4FF);
  static const Color indexBgColor = Color(0xFFF7FAFC);
  static const Color blue = Color(0xFF3182CE);
  static const Color tabBorder = Color(0xFFCBD5E0);
  static const Color tabSelection = Color(0xFF81E6D9);
}